set colorcolumn=+0
set cursorline
set fillchars=fold:-
set foldenable foldmethod=syntax foldlevelstart=4
set hidden
set ignorecase smartcase
set list listchars=trail:·,tab:×░
set notimeout
set number
set signcolumn=yes
set shiftwidth=0
set updatetime=100
set wildmode=longest:full


nnoremap <silent> <C-L> <C-L>:nohlsearch<CR>




"   Change indent style on the fly
"   War, war never changes..

nnoremap <leader>int :set sw=0 noet<CR>
nnoremap <leader>ins :set sw=4   et<CR>




"   NYC Vim Meetup 2016: Max Cantor "How to do 90% of what plugins do"
"   https://www.youtube.com/watch?v=XA2WjJbmmoM&t=6m30s

set path+=**




"   Save the active file if changes were made
"   https://stackoverflow.com/questions/3446320/

nnoremap <silent> <C-S>      :update<CR>
inoremap <silent> <C-S> <C-O>:update<CR>




"   Expand the active file directory
"   Neil Drew (2016). Practical Vim, pp.101

cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'




"   Manage plugins
"   https://github.com/junegunn/vim-plug

call plug#begin ('$HOME/.config/nvim/plugged')


Plug 'https://github.com/airblade/vim-gitgutter'
Plug 'https://github.com/tpope/vim-fugitive'


Plug 'https://github.com/junegunn/fzf.vim'

nnoremap gs :Files<CR>
nnoremap gl :Buffers<CR>


Plug 'https://github.com/w0rp/ale'

let g:ale_c_parse_compile_commands = 1

nmap <silent> <C-k> <Plug>(ale_previous)
nmap <silent> <C-j> <Plug>(ale_next)


Plug 'https://github.com/ntpeters/vim-better-whitespace'


call plug#end ()

