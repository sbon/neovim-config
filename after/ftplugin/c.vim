setlocal colorcolumn=80
setlocal foldcolumn=4                   " Show an indicator of open and closed folds
setlocal formatoptions-=ro              " Disable auto-insertion of comment leader
setlocal textwidth=0




" Disable indent inside a namespace
" Disable indent of scope declarations (public, private, protected)

setlocal cinoptions=N-s,g0

